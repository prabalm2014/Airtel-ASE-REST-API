<?php
  $id = $_GET['id'];
  $url = "http://127.0.0.1/airteras/subscriber/$id";
  $response = file_get_contents($url);
  $json = json_decode($response,true);
    foreach ($json as $key => $jsons) 
    {
      $srid = $json[$key]['id'];
      $keyword = $json[$key]['keyword'];
      $scode = $json[$key]['shortcode'];
      $istatus = $json[$key]['initial_status'];
      $pstatus = $json[$key]['previous_status'];
      $cstatus = $json[$key]['current_status'];
      $fstatus = $json[$key]['final_status'];
    }
    if(!empty($_POST["submit"]))
    {
        $data = array(
          "ids" => $_POST["ids"],
          "keyword" => $_POST["keyword"],
          "shcode" => $_POST["shcode"],
          "instatus" => $_POST["instatus"],
          "pstatus" => $_POST["pstatus"],
          "cstatus" => $_POST["cstatus"],
          "fstatus" => $_POST["fstatus"]
        );                                                                    
        $data_string = json_encode($data);                                                                                   
                                                                                                                     
      $ch = curl_init('http://127.0.0.1/airteras/subscriberupdate/$srid');                                                                      
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
      'Content-Type: application/json',                                                                                
      'Content-Length: ' . strlen($data_string))                                                                       
      );                                                                                                                  
      $result = curl_exec($ch);
      echo "Server : $result";
      header('Location: subscriber.php');
      exit();
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Update Service</title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="container">
    <h1>Update Service</h1>
    <div class="row">
        <form method="post" action="">
          <div class="col-md-6">
          		<div class="form-group">
                  	<label>ID:</label>
                  	<input type="text" name="ids" class="form-control" required readonly value="<?php echo $srid;?>">
              	</div>
              <div class="form-group">
                  <label>Keyword:</label>
                  <input type="text" name="keyword" class="form-control" required value="<?php echo $keyword;?>">
              </div>
              <div class="form-group">
                  <label>Short Code:</label>
                  <input type="text" name="shcode" class="form-control" required value="<?php echo $scode;?>">
              </div>
              <div class="form-group">
                  <label>Initial Status:</label>
                  <input type="text" name="instatus" class="form-control" required value="<?php echo $istatus;?>">
              </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
                  <label>Previous Status:</label>
                  <input type="text" name="pstatus" class="form-control" required value="<?php echo $pstatus;?>">
              </div>
            <div class="form-group">
                  <label>Current Status:</label>
                  <input type="text" name="cstatus" class="form-control" required value="<?php echo $cstatus;?>">
              </div>
              <div class="form-group">
                  <label>Final Status:</label>
                  <input type="text" name="fstatus" class="form-control" required value="<?php echo $fstatus;?>">
              </div>
              <input type="submit" name="submit" class="btn btn-default" value="Update"><a href="index.php" align="center">Back</a>
          </div>
      </form> 
    </div>
</div>
    
</body>
</html>
