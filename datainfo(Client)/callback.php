<?php
	$url = "http://127.0.0.1/airteras/callback/";
	$response = file_get_contents($url);
	//global $json;
	$json = json_decode($response,true);
	
?>
<!DOCTYPE html>
<html>
<head>
	<title>Callback List Report</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<div class="container-fluid">
		<div class="panel panel-default">
			<div class="panel-heading"><h1>Callback List Report</h1></div>
			<div class="panel-body">
				<table class="table table-striped">
			    <thead>
				    <tr>
				       	<th>ID</th>
				       	<th>Msisdn</th>
				       	<th>Service ID</th>
				       	<th>Service Type</th>
				       	<th>Plan ID</td>
				       	<th>Error Code</th>
				       	<th>Operation</th>
				       	<th>Result</th>
				       	<th>Trans ID</th>
				       	<th>Content ID</th>
				       	<th>Category</th>
				       	<th>Charge Amount</th>
				       	<th>Applied Plan</th>
				       	<th>End Date</th>
				       	<th>Validity Days</th>
				       	<th>Remarks</th>
				       	<th>Incomming Time</th>
				       	<th>Raw Data</th>
				       	<th>Actions</th>
				    </tr>
			    </thead>
			    <tbody>
			    <?php 
			    //$json = json_decode($response,true);
			    foreach ($json as $key => $jsons) 
				{?>
			 		<tr>
						<td><?php echo $json[$key]['id']; ?></td>
						<td><?php echo $json[$key]['msisdn']; ?></td>
						<td><?php echo $json[$key]['serviceId']; ?></td>
						<td><?php echo $json[$key]['serviceType']; ?></td>
						<td><?php echo $json[$key]['planId']; ?></td>
						<td><?php echo $json[$key]['errorCode']; ?></td>
						<td><?php echo $json[$key]['operation']; ?></td>
						<td><?php echo $json[$key]['result']; ?></td>
						<td><?php echo $json[$key]['transId']; ?></td>
						<td><?php echo $json[$key]['contentId']; ?></td>
						<td><?php echo $json[$key]['category']; ?></td>
						<td><?php echo $json[$key]['chargeAmount']; ?></td>
						<td><?php echo $json[$key]['appliedPlan']; ?></td>
						<td><?php echo $json[$key]['endDate']; ?></td>
						<td><?php echo $json[$key]['validityDays']; ?></td>
						<td><?php echo $json[$key]['remarks']; ?></td>
						<td><?php echo $json[$key]['incomming_time']; ?></td>
						<td><?php echo $json[$key]['raw_data']; ?></td>
						<td><a href="http://127.0.0.1/airteras/callbackdelete/<?php echo $json[$key]['id']; ?>">Delete</a></td>
				<?php
				}
				?>
			    </tbody>
			  	</table>
			  	<a href="index.php" align="center">Back</a>
			</div>
		</div>
  	</div>
</body>
</html>


