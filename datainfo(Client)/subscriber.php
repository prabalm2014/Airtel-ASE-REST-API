<?php
	$url = "http://127.0.0.1/airteras/subscriber/";
	$response = file_get_contents($url);
	//global $json;
	$json = json_decode($response,true);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Subscribers List Update</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<div class="container-fluid">
		<div class="panel panel-default">
			<div class="panel-heading"><h1>Subscribers List Update</h1></div>
			<div class="panel-body">
				<table class="table table-striped">
			    <thead>
				    <tr>
				       	<th>ID</th>
				       	<th>Msisdn</th>
				       	<th>Plan ID</th>
				       	<th>Keyword</th>
				       	<th>Short Code</th>
				       	<th>Initial Status</th>
				       	<th>Previous Status</th>
				       	<th>Current Status</th>
				       	<th>Final Status</th>
				       	<th>Incomming Time</th>
				       	<th>Last Update Time</th>
				       	<th>Status Update Time</th>
				       	<th>Service Type</th>
				       	<th>Remarks</th>
				       	<th>Count</th>
				       	<th>Actions</th>
				    </tr>
			    </thead>
			    <tbody>
			    <?php
			    foreach ($json as $key => $jsons)  
    			{?>
					<tr>
						<td><?php echo $json[$key]['id']; ?></td>
						<td><?php echo $json[$key]['msisdn']; ?></td>
						<td><?php echo $json[$key]['plan_id']; ?></td>
						<td><?php echo $json[$key]['keyword']; ?></td>
						<td><?php echo $json[$key]['shortcode']; ?></td>
						<td><?php echo $json[$key]['initial_status']; ?></td>
						<td><?php echo $json[$key]['previous_status']; ?></td>
						<td><?php echo $json[$key]['current_status']; ?></td>
						<td><?php echo $json[$key]['final_status']; ?></td>
						<td><?php echo $json[$key]['incomming_time']; ?></td>
						<td><?php echo $json[$key]['last_update_time']; ?></td>
						<td><?php echo $json[$key]['status_update_time'];  ?></td>
						<td><?php echo $json[$key]['service_type'];  ?></td>
						<td><?php echo $json[$key]['remarks']; ?></td>
						<td><?php echo $json[$key]['count']; ?></td>
						<td><a href="subscriber_update.php?id=<?php echo $json[$key]['id']; ?>">Update</a> | <a href="http://127.0.0.1/airteras/subscriberdelete/<?php echo $json[$key]['id']; ?>">Delete</a></td>
					</tr>
					<?php
				}?>
			    </tbody>
			  	</table>
			  	<a href="index.php" align="center">Back</a>
			</div>
		</div>
  	</div>
</body>
</html>