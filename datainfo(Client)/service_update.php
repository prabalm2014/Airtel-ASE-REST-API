<?php
	$id = $_GET['id'];
	$url = "http://127.0.0.1/airteras/service/$id";
	$response = file_get_contents($url);
	$json = json_decode($response,true);
    foreach ($json as $key => $jsons) 
  	{
  		$srid = $json[$key]['id'];
  		$sname = $json[$key]['service_name'];
  		$keyword = $json[$key]['keyword'];
  		$bprice = $json[$key]['base_price'];
  		$scode = $json[$key]['shortcode'];
  		$snode = $json[$key]['service_node'];
  		$pid = $json[$key]['plan_id'];
  		$stype = $json[$key]['service_type'];
  		$actday = $json[$key]['active_days'];
  		$sid = $json[$key]['service_id'];
  	}
  	if(!empty($_POST["submit"]))
  	{
        $data = array(
          "ids" => $_POST["ids"],
          "sname" => $_POST["sname"],
          "keyword" => $_POST["keyword"],
          "bprice" => $_POST["bprice"],
          "shcode" => $_POST["shcode"],
          "snode" => $_POST["snode"],
          "pid" => $_POST["pid"],
          "stype" => $_POST["stype"],
          "activday" => $_POST["activday"],
          "sid" => $_POST["sid"]
        );                                                                    
        $data_string = json_encode($data);                                                                                   
                                                                                                                     
      $ch = curl_init('http://127.0.0.1/airteras/serviceupdate/$srid');                                                                      
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
      'Content-Type: application/json',                                                                                
      'Content-Length: ' . strlen($data_string))                                                                       
      );                                                                                                                  
      $result = curl_exec($ch);
      echo "Server : $result";
      header('Location: service.php');
      exit();
  	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Update Service</title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="container">
    <h1>Update Service</h1>
    <div class="row">
        <form method="POST" action="">
          <div class="col-md-6">
          		<div class="form-group">
                  	<label>ID:</label>
                  	<input type="text" name="ids" class="form-control" required readonly value="<?php echo $srid;?>">
              	</div>
              <div class="form-group">
                  <label>Service Name:</label>
                  <input type="text" name="sname" class="form-control" required value="<?php echo $sname;?>">
              </div>
              <div class="form-group">
                  <label>Keyword:</label>
                  <input type="text" name="keyword" class="form-control" required value="<?php echo $keyword;?>">
              </div>
              <div class="form-group">
                  <label>Base Price:</label>
                  <input type="text" name="bprice" class="form-control" required value="<?php echo $bprice;?>">
              </div>
              <div class="form-group">
                  <label>Short Code:</label>
                  <input type="text" name="shcode" class="form-control" required value="<?php echo $scode;?>">
              </div>
              <div class="form-group">
                  <label>Service Node:</label>
                  <input type="text" name="snode" class="form-control" required value="<?php echo $snode;?>">
              </div>
          </div>
          <div class="col-md-6">
          <div class="form-group">
                  <label>Plan ID:</label>
                  <input type="text" name="pid" class="form-control" required value="<?php echo $pid;?>">
              </div>
              <div class="form-group">
                  <label>Service Type:</label>
                  <input type="text" name="stype" class="form-control" required value="<?php echo $stype;?>">
              </div>
              <div class="form-group">
                  <label>Active Day:</label>
                  <input type="text" name="activday" class="form-control" required value="<?php echo $actday;?>">
              </div>
              
              <div class="form-group">
                  <label>Service ID:</label>
                  <input type="text" name="sid" class="form-control" required value="<?php echo $sid;?>">
              </div>
              <input type="submit" name="submit" class="btn btn-default" value="Submit"><a href="service.php" align="center">Back</a>
          </div>
      </form> 
    </div>
</div>
    
</body>
</html>

<!--http://127.0.0.1/airteras/serviceupdate/<?php //echo $srid;?>-->