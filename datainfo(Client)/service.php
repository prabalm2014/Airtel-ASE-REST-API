<?php
	$url = "http://127.0.0.1/airteras/service/";
	$response = file_get_contents($url);
	//global $json;
	$json = json_decode($response,true);
	
?>
<!DOCTYPE html>
<html>
<head>
	<title>Service List Report</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<div class="container-fluid">
		<div class="panel panel-default">
			<div class="panel-heading"><h1>Service List Report</h1></div>
			<div class="panel-body">
				<table class="table table-striped">
			    <thead>
				    <tr>
				       	<th>ID</th>
				       	<th>Service Name</th>
				       	<th>Keyword</th>
				       	<th>Base Price</th>
				       	<th>Short Code</th>
				       	<th>Service Node</th>
				       	<th>Plan ID</th>
				       	<th>Service Type</th>
				       	<th>Plan Description</th>
				       	<th>Active Day</th>
				       	<th>Active?</th>
				       	<th>Service ID</th>
				       	<th>Action</th>
				    </tr>
			    </thead>
			    <tbody>
			    <?php 
			    //$json = json_decode($response,true);
			    foreach ($json as $key => $jsons) 
				{?>
			 		<tr>
						<td><?php echo $json[$key]['id']; ?></td>
						<td><?php echo $json[$key]['service_name']; ?></td>
						<td><?php echo $json[$key]['keyword']; ?></td>
						<td><?php echo $json[$key]['base_price']; ?></td>
						<td><?php echo $json[$key]['shortcode']; ?></td>
						<td><?php echo $json[$key]['service_node']; ?></td>
						<td><?php echo $json[$key]['plan_id']; ?></td>
						<td><?php echo $json[$key]['service_type']; ?></td>
						<td><?php echo $json[$key]['plan_desc']; ?></td>
						<td><?php echo $json[$key]['active_days']; ?></td>
						<td><?php echo $json[$key]['is_active']; ?></td>
						<td><?php echo $json[$key]['service_id']; ?></td>
						<td><a href="service_update.php?id=<?php echo $json[$key]['id']; ?>">Update</a> | <a href="http://127.0.0.1/airteras/servicedelete/<?php echo $json[$key]['id']; ?>">Delete</a></td>
					</tr>
				<?php
				}
				?>
			    </tbody>
			  	</table>
			  	<a href="index.php" align="center">Back</a>
			</div>
		</div>
  	</div>
</body>
</html>


