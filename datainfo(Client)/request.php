<?php
	$url = "http://127.0.0.1/airteras/request/";
	$response = file_get_contents($url);
	//global $json;
	$json = json_decode($response,true);
	
?>
<!DOCTYPE html>
<html>
<head>
	<title>Request Logs Report</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading"><h1>Request Logs Report</h1></div>
			<div class="panel-body">
				<table class="table table-striped" style="word-wrap:break-word;">
			    <thead>
				    <tr>
				       	<th>ID</th>
				       	<th>Msisdn</th>
				       	<th>Keyword</th>
				       	<th>Plan ID</th>
				       	<th>Request Type</th>
				       	<th>Request Data</th>
				       	<th>Response Data</th>
				       	<th>Hit Time</th>
				       	<th>Response Time</th>
				    </tr>
			    </thead>
			    <tbody>
			    <?php
			    foreach ($json as $key => $jsons)
    			{?>
					<tr>
						<td><?php echo $json[$key]['id']; ?></td>
						<td><?php echo $json[$key]['msisdn']; ?></td>
						<td><?php echo $json[$key]['keyword']; ?></td>
						<td><?php echo $json[$key]['plan_id']; ?></td>
						<td><?php echo $json[$key]['request_type']; ?></td>
						<td><?php echo $json[$key]['request_data']; ?></td>
						<td><?php echo $json[$key]['response_data']; ?></td>
						<td><?php echo $json[$key]['hittime']; ?></td>
						<td><?php echo $json[$key]['response_time']; ?></td>
					</tr>
				<?php
				}?>
			    </tbody>
			  	</table>
			  	<a href="index.php" align="center">Back</a>
			</div>
		</div>
  	</div>
</body>
</html>