<?php

require_once(dirname(__FILE__) . '/config.php');

class Subscriber{

	public static function query($sql){
		$conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DB);
		$res = $conn->query($sql);
		$conn->close();
		Subscriber::formatResults($res);
	}

	public static function formatResults($res){
		$results = array();
		foreach($res as $r){
			array_push($results, $r);
		}
		echo json_encode($results);
	}

}

$username = $_GET['username'];
if(!empty($username))
{
	Subscriber::query("SELECT * FROM subscribers WHERE id = '$username'");
}
else
{
	Subscriber::query("SELECT * FROM subscribers");
}

?>