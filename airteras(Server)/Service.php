<?php

require_once(dirname(__FILE__) . '/config.php');

class Service{

	public static function query($sql){
		$conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DB);
		$res = $conn->query($sql);
		$conn->close();
		Service::formatResults($res);
	}

	public static function formatResults($res){
		$results = array();
		foreach($res as $r){
			array_push($results, $r);
		}
		echo json_encode($results);
	}

}

$username = $_GET['username'];
if(!empty($username))
{
	Service::query("SELECT * FROM service_list WHERE id = '$username'");
}
else
{
	Service::query("SELECT * FROM service_list");
}

?>
